<?php
/*
Template Name: Receptai pagal tipa

*/

?>
 
<?php get_header(); ?>
 <?php get_sidebar('kaire'); ?>
 			<div id="content">
			<div id="breadcrumb">
				<a href="<?php bloginfo( 'url' );?> " >Pradžia</a>  &rsaquo; 	<a href="/visi-receptai/" >Receptai</a>  &rsaquo;  <a href=" <?php get_the_title() ?> " ><?php the_title()?></a>
			
</div>
			<div class="bloko_pavadinimas">	Receptai pagal tipą </div>
			

<ul class="taxonomyfor">
<?php
$args=array(
  'orderby' => 'name',
  'order' => 'ASC',
  'taxonomy' => 'receptu-tipas'
  );
$categories=get_categories($args);
  foreach($categories as $category) { 
?>
<li>


 <a href="http://jokuboreceptai.lt/receptu-tipas/<?php echo $category->slug ?>" title="<?php echo $category->name ;?> " ><?php echo $category->name .' ('. $category->count . ')' ?></a>
	<?php
	} 
	?>
</li>
</ul>
 </div>
 
 
<?php get_sidebar(); ?>
<?php get_footer(); ?>
 
