<?php
/*
Template Name: Receptai (meniu)

*/

?>
 
<?php get_header(); ?>
 <?php get_sidebar('kaire'); ?>
 			<div id="content">
									<div id="breadcrumb">
	<a href="<?php bloginfo( 'url' );?> " >Pradžia</a>  &rsaquo;  <a href=" <?php get_the_title() ?> " ><?php the_title()?></a>
			
</div>
				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
			<div class="bloko_pavadinimas">	<?php the_title(); ?></div>
			<div id="trumpas_aprasymas1">
<?php the_content(); ?>
</div>
<div class="bloko_pavadinimas">Mėgstamiausi receptai </div>
<div class="receptai_tituliniam">
<?php geriausi_receptai(); ?>	
<div class="po_foto"> <a href="/visi-receptai/top-20/">Daugiau mėgstamiausių receptų &rsaquo;&rsaquo;</a></div>
</div>

<div class="bloko_pavadinimas">Naujausi receptai</div>
<div class="receptai_tituliniam">
<?php geriausi_receptai(); ?>	
<div class="po_foto"> <a href="/visi-receptai/naujausi-receptai">Daugiau naujausių receptų &rsaquo;&rsaquo;</a></div>
</div>


	<?php endwhile; // end of the loop. ?>
 </div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
 
