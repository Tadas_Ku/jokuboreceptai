<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
<?php get_sidebar('kaire'); ?>
			<div id="content" >
			<!-- <script>utmx_section("testas")</script> -->
			<div id="breadcrumb1">
	<a href="<?php bloginfo( 'url' );?> " >Pradžia</a>  &rsaquo;  <a href=" <?php get_the_title() ?> " ><?php the_title()?></a>
			
</div>
<div itemscope itemtype="http://data-vocabulary.org/Recipe" >
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>                      
<?php $meta_values = get_post_meta(get_the_ID(), "_my_meta", true); ?>

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<h1 itemprop="name" class="entry-title"><?php the_title(); ?></h1>
					<div itemprop="summary" id="trumpas_aprasymas"><?php echo get_post_meta($post->ID, 'aprasymas', true);?></div>
					
					<div id="info_pries_recepta">
						<div class="fblike_time">
							<div id="fb-root"></div>
							<script src="http://connect.facebook.net/en_US/all.js#appId=247022018660467&amp;xfbml=1">
							</script><fb:like href="" send="false" data-colorscheme="light" layout="button_count" width="100" show_faces="false" font="">
							</fb:like>
							
							<div id="laikas"></div>
							<!-- paruosimo laikas  -->
							<div class="prepTime">
								<p><?php  echo "Paruošimo ".esc_html(get_post_meta(get_the_ID(), 'hcf_preptime', true ))."min. | Gaminimo"; ?> </p>
							</div>
							<!-- paruosimo laikas  -->

							<div id="laikas_min">
							<?php echo "&nbsp;".'<time  datetime="PT'.date('G', mktime(0,get_post_meta($post->ID, 'gaminimo_laikas', true))).'H'.date('i', mktime(0,get_post_meta($post->ID, 'gaminimo_laikas', true))).'M"  itemprop="totalTime">'.get_post_meta($post->ID, 'gaminimo_laikas', true);?></time> min.</div>
						
						</div>


						<!-- <div itemprop="author" class="vcard author summary-tag"> -->
							<!-- <span class="fn"><?php //the_author(); ?></span> -->
						<!-- </div> -->
						<?php
						//$date = the_date('Y-m-d',null,null,FALSE);
						?>
						
						<time class="date updated summary-tag"  datetime="<?php echo $date; ?>" itemprop="published"><?php echo $date; ?></time>
						<div id="reitingas">
							<?php if(function_exists('the_ratings')) { the_ratings(); } ?>
						</div>	
					</div>

					<div id="info_pries_recepta_mobile">
						<div class="mobile_rating">
							<div id="fb-root"></div>
							<script src="https://connect.facebook.net/en_US/all.js#appId=247022018660467&amp;xfbml=1"></script>
							<fb:like href="" send="false" data-colorscheme="light" layout="button_count" width="100" show_faces="false" font=""></fb:like>
							<div id="reitingas">
							<?php if(function_exists('the_ratings')) { the_ratings(); } ?>
						</div>

						</div>
						
						<div class="prepare_time_mobile">
							<div id="laikas"></div>
							<div class="prepTime">
								<p><?php  echo "Paruošimo ".esc_html(get_post_meta(get_the_ID(), 'hcf_preptime', true ))."min. | Gaminimo"; ?> </p>
							</div>
							<div id="laikas_min">
								<?php echo "&nbsp;".'<time  datetime="PT'.date('G', mktime(0,get_post_meta($post->ID, 'gaminimo_laikas', true))).'H'.date('i', mktime(0,get_post_meta($post->ID, 'gaminimo_laikas', true))).'M"  itemprop="totalTime">'.get_post_meta($post->ID, 'gaminimo_laikas', true);?></time> min.
							</div>	
						</div>

					
					</div>




							<div class="entry-content">
	<div id="first">				
	<div id="tools" >	
<div class="tool_dalintis"> </div>	
<a href="http://www.facebook.com/share.php?u=<?php echo get_permalink( $post->ID ); ?> " title="Dalintis receptu per Facebook" target="_blank" class="tool_fb"></a>		
<a href="/pop-up/?id=<?php the_ID(); ?>" title="Spausdinti receptą" id="tool_print"></a>	
<a href="/pop-up/?id=<?php the_ID(); ?>" title="Skaityti atskirame lange " id="tool_popup"></a>	
</div>
		<div id="konkretus_recepto_foto">	
				
<?php
global $thumbnail_id;
$imageArray = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail_name' );
$imageURL = $imageArray[0]; // here's the image url
preg_match ('/src="(.*)" class/',$thumbnail_id,$link);
?>
<!-- <a id="lightm"title="<?php //the_title(); ?>" href="<?php //echo $imageURL; ?>" alt="test" > -->
<?php 
// laikinai 
$date = the_date('Y-m-d',null,null,FALSE);


// $date[2] patikrina ar reveptas yra senesnis nei 202* metu jei taip spausdinam tik featured imaga, veliau reik pakeisti patikrinima kad jei yra galerija spausdina galerija
// jei jos neta spausdina thumbnail, tai

if ($date[2] != 2) {
	?><div class="slider"> <?php
	the_post_thumbnail(array(200,300),array('itemprop'=>'photo', 'class' => 'sliderimage'));
	
	?></div>
	<!-- <div id="caption">+</div> -->
	<?php
} ?>
<!-- </a> -->

<div class="slidercontainer">
	<?php 
	if ($date > 2021-02-01) {
		?> 
		<!-- <div class="slider"> -->
		<!-- <a id="light"title="<?php //the_title(); ?>" href="<?php //echo $imageURL; ?>" > -->
		<?php
//	the_post_thumbnail(array(200,300),array('itemprop'=>'', 'class' => 'sliderimage', 'onclick'=>'currentSlide(0)'));
		?>
		<!-- </a> -->
		<!-- </div>  -->
		<?php
		$galleries = get_post_galleries_images( $post );
		// Loop through all galleries found
		$count = 1;

		foreach( $galleries as $gallery ) {
			// Loop through each image in each gallery
			foreach( $gallery as $image ) {
				

				
				//echo '<div class="slider"><a id="light" title="test" href="'.$image.'"><img class ="sliderimage" src="'.$image.'" ></a></div>';
				echo '<div class="slider"><img class ="sliderimage" src="'.$image.'" ></div>';
				$count ++;

			}
		}
		echo '<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
			  <a class="next" onclick="plusSlides(1)">&#10095;</a>';
	}

	?>
	
</div>

<script>
	var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}


function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("slider");
 // var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
//   for (i = 0; i < dots.length; i++) {
//     dots[i].className = dots[i].className.replace(" active", "");
//   }
  slides[slideIndex-1].style.display = "block";
  //dots[slideIndex-1].className += " active";
 // captionText.innerHTML = dots[slideIndex-1].alt;
}

</script>

		</div>	
		</div>
		<div class="trumpaInfo">
			<?php 
					$blocks = parse_blocks($post->post_content);
			
					$string = $blocks[0]['innerHTML'];
					
					$textasPries = substr($string , 0, strpos($string , '<ol>'));
					echo $textasPries;
			
			?>

		</div>

		<div class="recepto_info">


		
	<div class="row_sudalys stulpelis" >

		
     <h2>Sudėtinės dalys</h2>
            <ul>
			<?php

			if($meta_values['recipeIngredient']){
				foreach ($meta_values['recipeIngredient'] as $key=>$value) {
			?>

 			<li  itemprop="ingredient" itemscope itemtype="http://data-vocabulary.org/RecipeIngredient">
 			<!-- <img width="5" height="10" src="/rec/wp-content/themes/receptukas/images/ul_li.png"> -->
			 <img src="/rec/wp-content/themes/receptukas/images/caret-right-solid.svg"> 
		
			<?php
				echo $meta_values['recipeIngredient'][$key]['amount'] . " ";
				include('kategorijos.php');
				foreach ($recipesUnits as $unitKey=>$unitValue) { 
					if($unitKey == $meta_values['recipeIngredient'][$key]['unit']){
					if($unitValue != "Stuk"){
					echo ' <a itemprop="name" href="' . get_category_link($unitValue) . '" title="' . sprintf( __( "View all posts in %s" ), $unitValue->name ) . '" ' . '>' . $unitValue->name.'</a>'.','.' ';

					}
					}
				} 
				echo '<span itemprop="unit">'.$meta_values['recipeIngredient'][$key]['title'].'</span>';
			?>
			<?php
			include('kategorijos.php');
			foreach ($vienetai as $unitKey=>$unitValue) { 
				if($unitKey == $meta_values['recipeIngredient'][$key]['vienetai']){
				if($unitValue != "Stuk"){
					echo $unitValue . " ";

				}
				}
			} 

			?>

			</li>
			<?php
				}
			}
			?>
			</ul>

			</div>


			<!-- <div id="tarpelis" style="margin-top:10px;"></div> -->

<div class="row_gaminimas stulpelis" >


					     <h2>Gaminimas</h2>
						<?php //the_content(); ?>
						<?php 				
// gaminimo aprasymas 
//$post_id = get_the_ID();
if ($date === 2021-02-01) {
	the_content();
	echo "test";
} else {
	// $galleries = get_post_galleries_images( $post );
	// // Loop through all galleries found
	// foreach( $galleries as $gallery ) {
	// 	// Loop through each image in each gallery
	// 	foreach( $gallery as $image ) {

	// 		//if ( $number == $count )
	// 		//return;
	// 		echo '<img src="'.$image.'">';
	// 		$count ++;

	// 	}
	// }

				//$post = get_post($post_id);
			
			$blocks = parse_blocks($post->post_content);
			
			$string = $blocks[0]['innerHTML'];
			
			//$list = ($date < 2021-02-01) ? "</ol>" : "</ol>";
			// nutrinti viska kas po <ol>
			$normalString = substr($string , 0, strpos($string , '</ol>'));
			$normalString = explode('<li>', $normalString );
			$elementsInArray = count($normalString); ?>
			<ul class="gaminimas">
			<?php	
			$i = 1;
			do {
				echo "<li><span>$i</span><div class='instruction'>$normalString[$i]</li>";
			    $i ++;
			} while ($i <= $elementsInArray -1 );
			?> 
			</ul>
			<?php

}

?>
</div>
</div>

					</div><!-- .entry-content -->
					
					<div id ="ad-single">	
<?php		
global $sa_options;
$settings = get_option( 'sa_options', $sa_options );
echo $settings['468_15']; 
 
 ?>
 </div>
								<!-- <div id="tarpelis" style="margin-top:20px;"></div> -->
							
					<div class="bloko_pavadinimas">Susiję receptai </div>
					<div id="relative"><?php susije_receptai() ?></div>
					
<?php /**if ( get_the_author_meta( 'description' ) ) : // If a user has filled out their description, show a bio on their entries  ?>
					<div id="autorius">
						<div id="fotke">
							<?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'twentyten_author_bio_avatar_size', 100 ) ); ?>
						</div><!-- #author-avatar -->
						
						<div id="autoriaus_vardas">
	<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" rel="author">
									<?php printf( __( '%s', 'twentyten' ), get_the_author() ); ?>
								</a>

							</div>
							<div id="autoriaus_aprasymas">
							<?php the_author_meta( 'description' ); ?>

						</div><!-- #author-description -->
					</div><!-- #entry-author-info -->
<?php endif; */ ?>

					</div>




				<?php comments_template( '', true ); ?>

<?php endwhile; // end of the loop. ?>

			</div><!-- #content -->

</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>