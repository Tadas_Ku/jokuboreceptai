
<?php
/*
Template Name: Priedu lentele

*/
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
 
get_header(); ?>
<!-- Google Website Optimizer Tracking Script -->
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['gwo._setAccount', 'UA-5504445-2']);
_gaq.push(['gwo._trackPageview', '/2153446143/goal']);
(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
<!-- End of Google Website Optimizer Tracking Script -->
<?php get_sidebar('kaire'); ?>
			<div id="content-priedai">
			
														<div id="breadcrumb">
				<a href="<?php bloginfo( 'url' );?> " >Pradžia</a>  &rsaquo;  <a href=" <?php get_the_title() ?> " ><?php the_title()?></a>
			
</div>
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<div class="bloko_pavadinimas">
	<h1 class="entry-title1"><?php the_title(); ?></h1>
	</div>
		<div id="tekstas">
	<?php the_content(); ?>
	</div>
	<?php endwhile; // end of the loop. ?>

			
		</div><!-- #container -->

<?php get_footer(); ?>
