<?php
/*
Template Name: POP-UP
*/
?>
<head>
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
</head>
<html>
<body>
	<div id="content">
<?php
	$post = get_post($_GET['id']);
?>
<?php if ($post) : ?>
	<?php setup_postdata($post); ?>
	<?php $meta_values = get_post_meta(get_the_ID(), "_my_meta", true); ?>
	
	<h1 class="entry-title"><?php the_title(); ?></h1>
	<div class="entry-content">
	<div id="first">				
	<div id="tools" >	
<div class="tool_dalintis"></div>	
<div class="tool_fb"> </div>	
<div id="tool_print"> </div>		
</div>
	<div id="konkretus_recepto_foto">	
	<?php the_post_thumbnail(array(200,300)); ?>
		</div>	
		</div>

<h2>Ingredientai:</h2>
            <ul>
<?php
  if($meta_values['recipeIngredient']){
    foreach ($meta_values['recipeIngredient'] as $key=>$value) {
?>
              <li><?php
  echo $meta_values['recipeIngredient'][$key][amount] . " ";
  include('kategorijos.php');
  foreach ($recipesUnits as $unitKey=>$unitValue) { 
    if($unitKey == $meta_values['recipeIngredient'][$key][unit]){
      if($unitValue != "Stuk"){
    echo ' <a href="' . get_category_link($unitValue) . '" title="' . sprintf( __( "View all posts in %s" ), $unitValue->name ) . '" ' . '>' . $unitValue->name.'</a>'.','.' ';

      }
    }
  } 
  echo $meta_values['recipeIngredient'][$key][title];
?>

<?php

  include('kategorijos.php');
  foreach ($vienetai as $unitKey=>$unitValue) { 
    if($unitKey == $meta_values['recipeIngredient'][$key][vienetai]){
      if($unitValue != "Stuk"){
        echo $unitValue . " ";

      }
    }
  } 

?>

</li>
<?php
    }
  }
?>
            </ul>
 <h2>Gaminimas:</h2>
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
					</div><!-- .entry-content -->
					

	
<?php endif; ?>