<?php
/*
Template Name: A-Z (receptai)

*/

?>
 
<?php get_header(); ?>
 <?php get_sidebar('kaire'); ?>
 			<div id="content">
						<div id="breadcrumb">
	<a href="<?php bloginfo( 'url' );?> " >Pradžia</a>  &rsaquo; 	<a href="/visi-receptai" >Receptai</a>  &rsaquo;  <a href=" <?php get_the_title() ?> " ><?php the_title()?></a>
			
</div>
			<div class="bloko_pavadinimas">Receptai abėcėlės tvarka A-Z</div>
			<div id="tarpas"></div>
		<div id="access2" > 
	<div class="menu-header">
	<ul class="menu">		
<?php 
wp_list_pages('depth=2&child_of=90&title_li=') ?>

</ul>
</div>
</div>
<div class="receptas_paieskoj">
<?php
$title =  get_the_title();
if ($title =='A-Z')
{
$raide = 'A';
}
else 
{
$raide = $title;
}
$paieska = $wpdb->get_results("SELECT * FROM $wpdb->posts
	WHERE `post_title` LIKE '$raide%' AND `post_status` = 'publish' AND `post_type` = 'receptai'");
if ($paieska) :
	foreach ($paieska as $key=>$post) :
	    if($key % 2) 
    {
     echo '<div class="receptas_su_info_1">' ;
    }
	else 
	{
     echo '<div class="receptas_su_info">' ;
	
	}
	
		setup_postdata($post);
?>
<?php $meta_values = get_post_meta(get_the_ID(), "_my_meta", true); ?>
			<div class="receptas_su_info_foto">
			            <?php echo get_the_post_thumbnail($page->ID, 'thumbnail'); ?>
			</div>
			<div class="receptas_su_info_pavadinimas">
			<a href="<?php the_permalink(); ?>"title="<?php the_title(); ?>">
<a href="<?php the_permalink() ?>">
<?php
$thetitle = $post->post_title;
$getlength = strlen($thetitle);
$thelength = 45;
echo substr($thetitle, 0, $thelength);
if ($getlength > $thelength) echo "...";
?>
</a>

</a>
			</div>
			<div class="info_juosta">
			<div class="laikas1"></div>
			<div class="laikas_min1">
					<?php echo get_post_meta($post->ID, 'gaminimo_laikas', true);?> min.
					</div>
                     <?php the_ratings_static() ?>    
					</div>
		<div class="receptas_su_info_aprasymas ">

<?php
$thetitle = get_post_meta($post->ID, 'aprasymas', true);
$getlength = strlen($thetitle);
$thelength = 200;
echo substr($thetitle, 0, $thelength);
if ($getlength > $thelength) echo "...";
?>
</div>	
</div>
<?php
	endforeach;
	
else :
?>
    <h2> Not Found</h2>
<?php endif;
 ?>

	</div>
 </div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
 
