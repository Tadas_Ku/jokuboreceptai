<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>
	</div><!-- #main -->
	</div><!-- #wrapper -->

	<div id="footer">
		<div id="colophon">

<?php get_sidebar('footer_turinys'); ?>

	<?php get_sidebar('footer_apie'); ?>	

<div id="footer_copyright">
<div class="footer_copyright_title">&copy;</div>
		<a href="<?php bloginfo( 'url' );?> " title="<?php bloginfo('name');?>"  ><?php bloginfo('name');?> | <?php echo date('Y'); ?> </a>
</div>

		</div><!-- #colophon -->
	</div><!-- #footer -->


<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
	
?>
</body>
</html>
