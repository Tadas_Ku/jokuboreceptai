<?php
/**
Straipsnio puslapis
 */
get_header(); ?>
<?php get_sidebar('kaire'); ?>
			<div id="content" >
			<div id="breadcrumb1">
	<a href="<?php bloginfo( 'url' );?> " >Pradžia</a>  &rsaquo;  <a href=" <?php get_the_title() ?> " ><?php the_title()?></a>
			
</div>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<? $meta_values = get_post_meta(get_the_ID(), "_my_meta", true); ?>
    	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<h1 class="entry-title"><?php the_title(); ?></h1>
			<div class="entry-content">
				
				<div id="konkretus_recepto_foto">	
					<?php
					$imageArray = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail_name' );
					$imageURL = $imageArray[0]; // here's the image url
					preg_match ('/src="(.*)" class/',$thumbnail_id,$link);
					?>
					<a id="light"title="<?php the_title(); ?>" href="<?php echo $imageURL; ?>" >
					<?php the_post_thumbnail(array(200,300)); ?></a>
				</div>	
			</div>

			<div id="tekstas1">	  
				<?php the_content(); ?>
				<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
			</div><!-- .entry-content -->
					
</div>






				<?php comments_template( '', true ); ?>

<?php endwhile; // end of the loop. ?>

			</div><!-- #content -->


<?php get_sidebar(); ?>
<?php get_footer(); ?>