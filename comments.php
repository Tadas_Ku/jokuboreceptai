<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains both current comments
 * and the comment form.  The actual display of comments is
 * handled by a callback to twentyten_comment which is
 * located in the functions.php file.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

			<div id="comments">



<?php if ( have_comments() ) : ?>
			<div class="bloko_pavadinimas">Atsiliepimai</div>


		
<?php 
$comments = get_comments(array('post_id' => $post->ID));
foreach ($comments as $key=>$comment) {
 if($key % 2) 
    {
     echo '<div id="komentaras1">' ;
    }
	else 
	{
     echo '<div id="komentaras">' ;
	
	}
	?>

		<div class="komentaro_autorius">
			<?php printf( __( '%s', 'twentyten' ), sprintf(  get_comment_author_link() ) ); ?> | <?php comment_date( ('n-j-Y  H:m')); ?> 
		</div><!-- .comment-author .vcard -->

		<div class="komentaro_tekstas">
		<div class="citata"></div>
		<?php comment_text(); ?></div>


<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
			<div class="navigation">
				<div class="nav-previous"><?php previous_comments_link( __( '<span class="meta-nav">&larr;</span> Older Comments', 'twentyten' ) ); ?></div>
				<div class="nav-next"><?php next_comments_link( __( 'Newer Comments <span class="meta-nav">&rarr;</span>', 'twentyten' ) ); ?></div>
			</div><!-- .navigation -->
<?php endif; // check for comment navigation ?>
	</div><!-- #comment-##  -->
	
	<?php
 }
	?>	



<?php else : // or, if we don't have comments:

	/* If there are no comments and comments are closed,
	 * let's leave a little note, shall we?
	 */
	if ( ! comments_open() ) :
?>
	<p class="nocomments"><?php _e( 'Comments are closed.', 'twentyten' ); ?></p>
<?php endif; // end ! comments_open() ?>

<?php endif; // end have_comments() ?>
<?php $args = array (
'comment_notes_before' => "",
'comment_notes_after' => "",
'title_reply' => 'Palikite atsiliepimą arba pasiūlymą',
'id_form' => 'atsiliepimas',
'id_submit' => 'siusti',
'label_submit' => __( '' ),
'fields' => array(
'author' => '<p class="comment-form-author">'  . '<input id="autorius_input" name="author" type="text" placeholder="Tavo vardas" size="20"/></p>',

),

'comment_field' => _x( '<img src="/wp-content/themes/receptukas/images/kabutes.svg"/>', 'noun' ) .'<p class="comment-form-comment"><label for="comment"></label><textarea id="comment" name="comment"  aria-required="true" placeholder="Atsiliepimas"></textarea></p>',
);?>

<?php comment_form( $args, $post_id ); ?>
</div>
<!-- 'author' => '<p class="comment-form-author">'  . '<input id="autorius_input" name="author" type="text" placeholder="Atsiliepimas" size="20" onfocus="if(this.value==\'Tavo vardas\')this.value=\'\';" onblur="if(this.value==\'\')this.value=\'Tavo vardas\';"  value="Tavo vardas" /></p>', -->


