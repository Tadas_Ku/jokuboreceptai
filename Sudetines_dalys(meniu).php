<?php
/*
Template Name: Sudėtinės-dalys(meniu)

*/

?>
 
<?php get_header(); ?>
 <?php get_sidebar('kaire'); ?>
 			<div id="content">
			
								<div id="breadcrumb">
				<a href="<?php bloginfo( 'url' );?> " >Pradžia</a>  &rsaquo; 	<a href="/sudetines-dalys/" >Sudėtinės dalys</a>  
			
</div>	
			
			<div class="bloko_pavadinimas"><h1>	Sudėtinės dalys</h1></div>


<?php
$args=array(
  'type' => 'receptai',
  'taxonomy'=> 'produktai',
  'orderby' => 'name',
  'order' => 'ASC'
  );
$categories=get_categories($args);
  foreach($categories as $key=>$category) {

    if($key % 2) 
    {
     echo '<div class="receptas_su_info11">' ;
    }
	else 
	{
     echo '<div class="receptas_su_info1">' ;
	
	}

  ?>
 
  
  		<div class="receptas_su_info_pavadinimas1">
 
 <a href="http://jokuboreceptai.lt/produktai/<?php echo $category->slug ?>" title="<?php echo $category->name ;?> " ><?php echo $category->name ?></a>
		
	
	</div>
			<div class="receptas_su_info_aprasymas1 "> 
			<?php
			$aprasymas =  $category->description ;
			$getlength = strlen($aprasymas);
            $thelength = 300;
			if ($aprasymas) {
echo substr($aprasymas, 0, $thelength);
if ($getlength > $thelength) echo "...";
}

			?>
			</div>
			</div>
			<?php
	} 
?>



 </div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
 

