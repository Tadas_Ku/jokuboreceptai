<?php
/*
Template Name: A-Z (maisto priedai)

*/

?>
 
<?php get_header(); ?>
 <?php get_sidebar('kaire'); ?>
 			<div id="content">
											<div id="breadcrumb">
				<a href="<?php bloginfo( 'url' );?> " >Pradžia</a>  &rsaquo; 	<a href="/maisto-priedai/" >Maisto priedai</a>  &rsaquo;  <a href=" <?php get_the_title() ?> " ><?php the_title()?></a>
			
</div>
			
			<div class="bloko_pavadinimas">Maisto priedai abėcėlės tvarka A-Z</div>
			<div id="tarpas"></div>
		<div id="access2" > 
	<div class="menu-header">
	<ul class="menu">		
<?php 
wp_list_pages('depth=2&child_of=1928&title_li=') ?>

</ul>
</div>
</div>
<div class="receptas_paieskoj">
<?php
$title =  get_the_title();
if ($title =='A-Z')
{
$raide = 'A';
}
else 
{
$raide = $title;
}

$paieska = $wpdb->get_col("SELECT * FROM $wpdb->posts
	WHERE `post_title` LIKE '$raide%' AND `post_status` = 'publish' AND `post_type` = 'maisto-priedai'");
	$args = array(
    'post__in' => $paieska,
    'paged' => get_query_var('paged'),
    'posts_per_page' => 10,
    'orderby' => 'post__in',
	'post_type'=>'maisto-priedai'
    );
query_posts($args); 

while ( have_posts() ) : the_post();
?>


<?php $meta_values = get_post_meta(get_the_ID(), "_my_meta", true); 

 if( $count++ % 2) 
    {
     echo '<div class="receptas_su_info1">' ;
    }
	else 
	{
     echo '<div class="receptas_su_info11">' ;
	
	}
	
	?>
			<div class="receptas_su_info_pavadinimas1">
			<a href="<?php the_permalink(); ?>"title="<?php the_title(); ?>">
<a href="<?php the_permalink() ?>">
<?php
$thetitle = $post->post_title;
$getlength = strlen($thetitle);
$thelength = 46;
echo substr($thetitle, 0, $thelength);
if ($getlength > $thelength) echo "...";
?>
</a>

</a>
			</div>
		<div class="receptas_su_info_aprasymas1 ">

 <?php the_excerpt(); ?> 
</div>		
			
	
			
</div>	


<?php endwhile; ?>
<div id="navigacija"><?php wp_pagenavi(); ?></div>
<?php
wp_reset_query();

?>
	</div>
 </div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
 
