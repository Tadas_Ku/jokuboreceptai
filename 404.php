<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<?php get_sidebar('kaire'); ?>

		<div id="content" >


<div class="bloko_pavadinimas">Receptas nerastas. Tačiau...</div>
<div class="bloko_pavadinimas1">Štai populiariausi receptai - galbūt jie slepia atsakymą! </div>
<div class="receptai_tituliniam">
<?php geriausi_receptai(); ?>	
<div class="po_foto"> <a href="/visi-receptai/top-20/">Daugiau mėgstamiausių receptų &rsaquo;&rsaquo;</a></div>
</div>


		
		
		

		</div><!-- #content -->


<?php get_sidebar(); ?>
<?php get_footer(); ?>