<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 <html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
<?php if(is_singular('receptai')):?>
<!-- Google Website Optimizer Control Script -->
<!-- <script>
function utmx_section(){}function utmx(){}
(function(){var k='2153446143',d=document,l=d.location,c=d.cookie;function f(n){
if(c){var i=c.indexOf(n+'=');if(i>-1){var j=c.indexOf(';',i);return escape(c.substring(i+n.
length+1,j<0?c.length:j))}}}var x=f('__utmx'),xx=f('__utmxx'),h=l.hash;
d.write('<sc'+'ript src="'+
'http'+(l.protocol=='https:'?'s://ssl':'://www')+'.google-analytics.com'
+'/siteopt.js?v=1&utmxkey='+k+'&utmx='+(x?x:'')+'&utmxx='+(xx?xx:'')+'&utmxtime='
+new Date().valueOf()+(h?'&utmxhash='+escape(h.substr(1)):'')+
'" type="text/javascript" charset="utf-8"></sc'+'ript>')})();
</script> -->
<!-- End of Google Website Optimizer Control Script -->
<!-- Google Website Optimizer Tracking Script -->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['gwo._setAccount', 'UA-5504445-2']);
  _gaq.push(['gwo._trackPageview', '/2153446143/test']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<!-- End of Google Website Optimizer Tracking Script -->

<?php endif; ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> <?php titlai()	?></title>
<meta name="description" content="<?php meta_descriptions()?>" />
<meta property="og:image" content="<?php ogg_image() ?>" />
<meta property="og:url" content="<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?> " />
<meta property="og:title" content=" <?php titlai()	?>" />

<meta property="og:image:alt" content="test" />
<meta property="og:type" content="test" />
<meta property="og:description" content="<?php meta_descriptions()?>" />
<meta property="fb:app_id" content="test" />


<meta content="width=device-width, initial-scale=1.0" name="viewport">
<link rel="icon" type="image/png" href="/rec/wp-content/themes/receptukas/images/favicon.ico"/>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="/rec/wp-content/themes/receptukas/style.css" /> 
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,700italic,400italic&subset=latin,cyrillic-ext,latin-ext,cyrillic' rel='stylesheet' type='text/css' />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
<!-- tado -->
<script src="https://kit.fontawesome.com/1da3636d1b.js" crossorigin="anonymous"></script>

<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,400;0,500;0,800;0,900;1,800&display=swap" rel="stylesheet">
<!-- tado -->

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
<!-- <script src="jquery-3.5.1.min.js"></script> -->
<script src="/rec/wp-content/themes/receptukas/jquery.js"></script>
	<script>
	//	!window.jQuery && document.write('<script src="/rec/wp-content/themes/receptukas/fancybox/jquery-1.4.3.min.js"><\/script>');
	</script>
	<!-- <script type="text/javascript" src="/rec/wp-content/themes/receptukas/fancybox/fancybox/jquery.fancybox-1.3.4.pack.js"></script> -->
	<!-- <link rel="stylesheet" type="text/css" href="/rec/wp-content/themes/receptukas/fancybox/fancybox/jquery.fancybox-1.3.4.css" media="screen" /> -->
		<!-- <script type="text/javascript">
		$(document).ready(function() {
			$("a#light").fancybox({
				'titlePosition'	: 'inside'
			});
		
		$("#tool_popup").fancybox({
'autoDimensions'	: false,
			'width'         		: 500,
			'height'        		: '100%',

				'autoScale'			: false,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe',
				'titleShow'         : false
			});
					});
	</script> -->
	<!--GA kodas -->
	<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-26712172-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();


</script>

</head>

<body <?php body_class(); ?> >
<!-- navigation  -->


<?php  include('navigation.php') ?>

<?php include('mobilenav.php') ?>

<div id="remas" class="<?php echo $remas_class; ?>">
	<div id="main" class="grid-container">