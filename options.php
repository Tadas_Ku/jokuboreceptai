<?php

// Default options values
$sa_options = array(
	'720_90' => '',
	'468_15' => '',
	'home_descr' => '',

);

if ( is_admin() ) : // Load only if we are viewing an admin page

function sa_register_settings() {
	// Register settings and call sanitation functions
	register_setting( 'sa_theme_options', 'sa_options', 'sa_validate_options' );
}

add_action( 'admin_init', 'sa_register_settings' );



function sa_theme_options() {
	// Add theme options page to the addmin menu
	add_theme_page( 'Receptuko nustatymai', 'Receptuko nustaymai', 'edit_theme_options', 'theme_options', 'sa_theme_options_page' );
}

add_action( 'admin_menu', 'sa_theme_options' );

// Function to generate options page
function sa_theme_options_page() {
	global $sa_options, $sa_categories, $sa_layouts;

	if ( ! isset( $_REQUEST['updated'] ) )
		$_REQUEST['updated'] = false; // This checks whether the form has just been submitted. ?>

	<div class="wrap">

	<?php screen_icon(); echo "<h2>" . get_current_theme() . __( ' nustatymai' ) . "</h2>";
	// This shows the page's name and an icon if one has been provided ?>

	<?php if ( false !== $_REQUEST['updated'] ) : ?>
	<div class="updated fade"><p><strong><?php _e( 'Options saved' ); ?></strong></p></div>
	<?php endif; // If the form has just been submitted, this shows the notification ?>

	<form method="post" action="options.php">

	<?php $settings = get_option( 'sa_options', $sa_options ); ?>
	
	<?php settings_fields( 'sa_theme_options' );
	/* This function outputs some hidden fields required by the form,
	including a nonce, a unique number used to ensure the form has been submitted from the admin page
	and not somewhere else, very important for security */ ?>

	<table class="form-table"><!-- Grab a hot cup of coffee, yes we're using tables! -->


	<tr valign="top"><th scope="row"><label for="720_90">Headerio reklama</label></th>
	<td>
	<textarea id="720_90" name="sa_options[720_90]" rows="5" cols="30"><?php echo stripslashes($settings['720_90']); ?></textarea>
	</td>
	</tr>	
	
	<tr valign="top"><th scope="row"><label for="468_15">Recepto puslapio reklama</label></th>
	<td>
	<textarea id="720_90" name="sa_options[468_15]" rows="5" cols="30"><?php echo stripslashes($settings['468_15']); ?></textarea>
	</td>
	</tr>
	
		<tr valign="top"><th scope="row"><label for="home_descr">Titulinio meta description</label></th>
	<td>
	<textarea id="home_descr" name="sa_options[home_descr]" rows="5" cols="30"><?php echo stripslashes($settings['home_descr']); ?></textarea>
	</td>
	</tr>

	</table>

	<p class="submit"><input type="submit" class="button-primary" value="Save Options" /></p>

	</form>

	</div>

	<?php
}

function sa_validate_options( $input ) {
	global $sa_options;

	$settings = get_option( 'sa_options', $sa_options );
	
	// We strip all tags from the text field, to avoid vulnerablilties like XSS
	$input['720_90'] = $input['720_90'] ;
	$input['468_15'] = $input['468_15'] ;	
	$input['home_descr'] = wp_filter_post_kses($input['home_descr']) ;
	return $input;
}

endif;  // EndIf is_admin()