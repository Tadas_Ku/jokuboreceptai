<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
<?php get_sidebar('kaire'); ?>
	
<div id="content" role="main">

											<div id="breadcrumb">
				<a href="<?php bloginfo( 'url' );?> " >Pradžia</a>  &rsaquo; <a href="">Paieškos rezultatai</a> &rsaquo; <a href=""> <?php printf(get_search_query() )?></a>
			
</div>

<h1 class="bloko_pavadinimas"><?php printf( __( 'Ieškota: %s', 'twentyten' ), '<b>' . get_search_query().'</b>' .'.' .' '. 'Rezultatai:' ); ?></h1>

<?php  if ( have_posts() ) : while ( have_posts() ) : the_post(); 

	?>
<?php $meta_values = get_post_meta(get_the_ID(), "_my_meta", true); ?>
<?php if (   has_post_thumbnail() ) { 
 if( $count++ % 2) 
    {
     echo '<div class="receptas_su_info">' ;
    }
	else 
	{
     echo '<div class="receptas_su_info_1">' ;
	
	}
	?>
			<div class="receptas_su_info_foto">
			    <?php echo get_the_post_thumbnail($page->ID, 'thumbnail'); ?>
			</div>
			<div class="receptas_su_info_pavadinimas">
				<a href="<?php the_permalink(); ?>"title="<?php the_title(); ?>">
					<a href="<?php the_permalink() ?>">
						<?php
						// $thetitle = $post->post_title;
						// $getlength = strlen($thetitle);
						// $thelength = 45;
						// echo substr($thetitle, 0, $thelength);
						// if ($getlength > $thelength) echo "...";
						?>
					</a>
				</a>
			</div>
			<div class="category_recipe_info">
				<?php $laikas= get_post_meta($post->ID, 'gaminimo_laikas', true);
				if ($laikas) {?>
				
				<div class="info_juosta">
					<!-- <div class="laikas1"></div> -->
					<div class="laikas_min1">
						<div id="time"></div>
						<?php echo $laikas;?>
						<div class="min">min.</div>
						<?php the_ratings() ?> 
					</div>
					<div class="category_recipe_title">
						<a href="<?php the_permalink() ?>">
								<?php
								$thetitle = $post->post_title;
								$getlength = strlen($thetitle);
								$thelength = 45;
								echo substr($thetitle, 0, $thelength);
								if ($getlength > $thelength) echo "...";
								?>
						</a>
					</div>

					<?php //the_ratings_static() ?>    
				</div>



						<?php } ?>
<div class="receptas_su_info_aprasymas"> 

						<?php
$thetitle = get_post_meta($post->ID, 'aprasymas', true);
$getlength = strlen($thetitle);
$thelength = 200;
if ($thetitle) {
echo substr($thetitle, 0, $thelength);
if ($getlength > $thelength) echo "...";
}
else {
 the_excerpt(); 
}
?>
</div>			

</div>	
			</div>


	<?php } 

else { 

 if( $count++ % 2) 
    {
     echo '<div class="receptas_su_info1">' ;
    }
	else 
	{
     echo '<div class="receptas_su_info11">' ;
	
	}

?>

			<div class="receptas_su_info_pavadinimas1">
			<a href="<?php the_permalink(); ?>"title="<?php the_title(); ?>">
<a href="<?php the_permalink() ?>">
<?php
$thetitle = $post->post_title;
$getlength = strlen($thetitle);
$thelength = 45;
echo substr($thetitle, 0, $thelength);
if ($getlength > $thelength) echo "...";
?>
</a>

</a>
			</div>
		<div class="receptas_su_info_aprasymas1 ">

 <?php the_excerpt(); ?> 
</div>		



	
			
</div>	

<?php } ?>
		
 <?php endwhile; else: ?>

<div class="bloko_pavadinimas">Nieko nerasta. Tačiau...</div>
<div class="bloko_pavadinimas1">Štai populiariausi receptai - galbūt jie slepia atsakymą! </div>
<div class="receptai_tituliniam">
<?php geriausi_receptai(); ?>	
<div class="po_foto"> <a href="/visi-receptai/top-20/">Daugiau mėgstamiausių receptų &rsaquo;&rsaquo;</a></div>
</div>
<?php endif; ?>
<div id="navigacija"><?php wp_pagenavi(); ?></div>
			</div><!-- #content -->


<?php get_sidebar(); ?>
<?php get_footer(); ?>
