<?php
/**
 * The template for displaying Category Archive pages.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<?php get_sidebar('kaire'); ?>

			<div id="content" >
			<div id="breadcrumb">
				<a href="<?php bloginfo( 'url' );?> " >Pradžia</a>   &rsaquo;<?php breadcrum_for_category(); ?> &rsaquo; <a href=""><?php
					printf( __( '%s', 'twentyten' ), '<span>' . single_cat_title( '', false ) . '</span>' ); ?></a>
			</div>
			<h1 class="bloko_pavadinimas"><?php
					printf( __( '%s', 'twentyten' ), '<span>' . single_cat_title( '', false ) . '</span>' );
				?></h1>
		
			<div id="trumpas_aprasymas">
							<?php
					$category_description = category_description();
					if ( ! empty( $category_description ) )
						echo '<div class="archive-meta">' . $category_description . '</div>';
						?>
						</div>
						<div class="bloko_pavadinimas"><?php
					printf( __( '%s', 'twentyten' ), '<span>' . single_cat_title( '', false ) . '</span>' );
				?>: susiję receptai</div>	
				
<?php while ( have_posts() ) : the_post(); ?>
<?php $meta_values = get_post_meta(get_the_ID(), "_my_meta", true); ?>
			<div class="receptas_su_info">
			<div class="receptas_su_info_foto">
			            <?php echo get_the_post_thumbnail($page->ID, 'thumbnail'); ?>
			</div>
			<div class="receptas_su_info_pavadinimas">
			<a href="<?php the_permalink(); ?>"title="<?php the_title(); ?>">
<a href="<?php the_permalink() ?>">
<?php
$thetitle = $post->post_title;
$getlength = strlen($thetitle);
$thelength = 45;
echo substr($thetitle, 0, $thelength);
if ($getlength > $thelength) echo "...";
?>
</a>

</a>
			</div>
			<div class="info_juosta">
			<div class="laikas1"></div>
			<div class="laikas_min1">
					<?php echo get_post_meta($post->ID, 'gaminimo_laikas', true);?> min.
					</div>
                     <?php the_ratings_static() ?>    
					</div>
		<div class="receptas_su_info_aprasymas ">

<?php
$thetitle = get_post_meta($post->ID, 'aprasymas', true);
$getlength = strlen($thetitle);
$thelength = 200;
echo substr($thetitle, 0, $thelength);
if ($getlength > $thelength) echo "...";
?>
</div>		
			
	
			
</div>	
	<?php endwhile; ?>
	<div id="navigacija"><?php wp_pagenavi(); ?></div>
</div>


<?php get_sidebar(); ?>
<?php get_footer(); ?>
