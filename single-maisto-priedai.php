<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
<?php get_sidebar('kaire'); ?>
			<div id="content">
			
						<div id="breadcrumb">
	<a href="<?php bloginfo( 'url' );?> " >Pradžia</a>  &rsaquo; 	<a href="/maisto-priedai/" >Maisto priedai</a>  &rsaquo;  <a href=" <?php get_the_title() ?> " ><?php the_title()?></a>
			
</div>
			
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<h1 class="entry-title1"><?php the_title(); ?></h1>
	
		<div id="tekstas1">
	<?php the_content(); ?>
	</div>
	<?php endwhile; // end of the loop. ?>

			
		</div><!-- #container -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
