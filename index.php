<?php get_header();?>

<?php get_sidebar('kaire');?>

<div id="content" class="grid-item"> 

<div id="slider"><?php 
//if(function_exists('wp_content_slider')) { wp_content_slider(); } 

echo do_shortcode('[smartslider3 slider="2"]');

?></div>

<div class="bloko_pavadinimas"><h2>Naujausi receptai</div>
<div class="receptai_tituliniam">
<?php naujausi_receptai(); ?>	
<!-- <div class="po_foto"> <a href="/visi-receptai/naujausi-receptai">Daugiau naujausių receptų &rsaquo;&rsaquo;</a></div> -->
</div>
<!-- generuojama bloga nuoroda jei href="/visi-receptai/naujausi-receptai" -->
<div class="po_foto"> <a href="visi-receptai/naujausi-receptai">Daugiau naujausių receptų &rsaquo;&rsaquo;</a></div>			
<div class="bloko_pavadinimas"><h1>Populiariausi receptai </h1></div>
<div class="receptai_tituliniam">
<?php geriausi_receptai(); ?>	
<!-- <div class="po_foto"> <a href="/visi-receptai/top-30/">Daugiau mėgstamiausių receptų &rsaquo;&rsaquo;</a></div> -->
</div>
<div class="po_foto"> <a href="visi-receptai/top-30/">Daugiau mėgstamiausių receptų &rsaquo;&rsaquo;</a></div>			
<div class="bloko_pavadinimas"><h2>Naujausi straipsniai</h2></div>
<div class="straipsniai">
<ul>
		<?php naujausi_straipsniai(); ?>	
</ul>
<!-- <div class="po_foto"> <a href="/straipsniai">Daugiau straipsnių apie maistą &rsaquo;&rsaquo;</a></div> -->
</div>
<div class="po_foto"> <a href="straipsniai">Daugiau straipsnių apie maistą &rsaquo;&rsaquo;</a></div>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?> 

