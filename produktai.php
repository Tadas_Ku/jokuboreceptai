<?php include('kategorijos.php'); ?>
<div class="my_meta_control">
  <p>
    <em>Iš viso produktų: <?php if (isset($meta['recipeIngredient'])) {
                                    echo count($meta['recipeIngredient']);
                                  } 
    
  //  echo count($meta['recipeIngredient']); ?></em><br />
<!--    <pre><?php // print_r($meta['recipeIngredient']); ?></pre> -->
  </p>

  <table cellpadding="0" cellpadding="0" id="ingredients">
    <tr>
	      <th class="colIngrUnit">Produktas</th>
      <th class="colIngrTitle">Kiekis</th>
	     <th class="colIngrAmount">Matavimo vienetas</th>
      <th class="colIngrActions"></th>
    </tr>


<?php 

// jei dar nera ingridiento tai paduodam tuscia array

    if (!isset($meta['recipeIngredient'])) {
      
      $meta =  array(
        'recipeIngredient' => array(
          array(
            "unit" =>'-1',
            "title"=>'',
            "vienetai"=> '-1'
          )
        )
      );
    }
    $SelectedIngredients = array();
    foreach ($meta['recipeIngredient'] as $key=>$value) {  
      //echo 'testes';     
?>
<!-- when recipeIngrediant exist -->
<div>
    <tr id="ingredient-<?php echo $key; ?>">
    <!-- <br> -->
    <!-- produktas -->

  
      <td class="colIngrUnit">
        <select name="_my_meta[recipeIngredient][<?php echo $key; ?>][unit]" id="_my_meta[recipeIngredient][<?php echo $key; ?>][unit]" class="ingredUnit">
          <option value="">Pasirinkti</option>

          <?php  

            foreach ($recipesUnits as $unitKey=>$unitValue) { ?>
            <option <?php if($meta['recipeIngredient'][$key]['unit'] == $unitKey){ echo " selected=\"selected\" ";} ?> 
  
               value="<?php echo $unitKey ?>"><?php echo $unitValue->name ?></option>
          <?php  } ?>
          
        </select>
        
      </td>
      <?php //var_dump($list);?>
      <!-- kiekis -->
      <td class="colIngrTitle">
            <input type="text" 
                   class="ingredTitle" 
                   name="_my_meta[recipeIngredient][<?php echo $key; ?>][title]" 
                   value="<?php if(!empty($meta['recipeIngredient'][$key]['title'])) echo $meta['recipeIngredient'][$key]['title']; ?>"/>
      </td>
	    <!-- matavimo vienetas -->
	    <td class="colIngrAmount">
         <select name="_my_meta[recipeIngredient][<?php echo $key; ?>][vienetai]" id="_my_meta[recipeIngredient][<?php echo $key; ?>][vienetai]" class="colIngrAmount">
           <option value="">Pasirinkti</option>
           <?php foreach ($vienetai as $unitKey=>$unitValue) { ?>
             <option <?php if($meta['recipeIngredient'][$key]['vienetai'] == $unitKey){ echo " selected=\"selected\" ";} ?> 
                           value="<?php echo $unitKey ?>"><?php echo $unitValue ?></option>
           <?php } ?>
        </select>
      </td>
	  <!-- delete -->
      <td class="colIngrActions">
        <button class="ingredientDelete">X </button>
      </td>
    </tr>
    </div>
<?php
    }
  // }
?>
 
  </table>
  <button id="ingredientAdd">Pridėti daugiau</button>
  <button id="ingredientPush">Sužymėti produktus</button>
  
  <script type="text/javascript">
  

    jQuery("button.ingredientDelete").on('click',  function() {
      if (jQuery("table#ingredients tr").length <= 2) {
        alert("Reikalingas bent vienas!");
      }
      else {
        // <?php 
        //   unset($meta['recipeIngredient'][1]);
        // ?> 
      //  alert("Deleted :)");
        jQuery(this).parent().parent().addClass("delete");
        jQuery(this).parent().parent().fadeOut('20000', function(){
          jQuery(this).find('input').val("");
          // arr.splice(2);
          // console.log(jQuery(this));

          // jQuery(this).find( "#ingredient-2" ).val();
          // console.log(jQuery(this).find('tr').val(""));
        });
      }
      return false;



    }); 

    jQuery("#ingredientPush").on('click', null, function() {
      jQuery('#produktaichecklist input').prop("checked", false );
      var reikalingiProduktai = []; 

      jQuery('#ingredients option:selected').each(function (j) {
                 reikalingiProduktai.push(jQuery(this).text());
      })

      // console.log(reikalingiProduktai);

      jQuery('#produktaichecklist li label').each(function (i) {
        var text = jQuery(this).text();

        jQuery(reikalingiProduktai).each(function(index, produktas){
          var manoString = produktas;
          if (jQuery.trim(text) === manoString ) {
              var values = jQuery('#produktaichecklist li label input');
              var idok = parseInt(values.get(i).value);
              jQuery('#in-produktai-' + idok).prop("checked", true );
              // jei rado nutraukti cikla ir eit kitur
            }
        })

      })
          return false;
    })


    jQuery("button#ingredientAdd").on('click', null, function() {
      var ingrLastID = parseInt(jQuery("table#ingredients tr").last().attr("id").replace("ingredient-", ""));
      var ingrNewID = parseInt(ingrLastID+1);

      var newRow =  '\
        <tr id="ingredient-' + ingrNewID + '">\
          <td class="colIngrUnit">\
            <select name="_my_meta[recipeIngredient][' + ingrNewID + '][unit]" id="_my_meta[recipeIngredient][' + ingrNewID + '][unit]" class="ingredUnit">\
              <option value="">Pasirinkti</option>\
<?php foreach ($recipesUnits as $unitKey=>$unitValue) { ?>
              <option value="<?php echo $unitKey ?>"><?php echo $unitValue->name ?></option>\
<?php } ?>
            </select>\
          </td>\
          <td class="colIngrTitle">\
            <input type="text" class="ingredTitle" name="_my_meta[recipeIngredient][' + ingrNewID + '][title]" value="">\
          </td>\
<td class="colIngrAmount">\
            <select name="_my_meta[recipeIngredient][' + ingrNewID + '][vienetai]" id="_my_meta[recipeIngredient][' + ingrNewID + '][vienetai]" class="colIngrAmount">\
              <option value="">Pasirinkti</option>\
<?php foreach ($vienetai as $unitKey=>$unitValue) { ?>
              <option value="<?php echo $unitKey ?>"><?php echo $unitValue ?></option>\
<?php } ?>
            </select>\
          </td>\
          <td class="colIngrActions">\
            <button class="ingredientDelete">X</button>\
          </td>\
        </tr>\
		';

//      alert(newRow)
//      alert("last ingredient id: " + ingrLastID + "\nnew ingredient id: " + ingrNewID)

      jQuery("table#ingredients tbody").append(newRow);
      return false;
    })

  </script>
</div>
