
<?php
/**
 * Template name: Maisto priedu lentele
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
<!-- End of Google Website Optimizer Tracking Script -->
<script language="JavaScript">
  function clearfield(field) {
			field.value=''
	}

  function clear_field(field) {
		if (field.value==field.defaultValue) {
			field.value=''
		}
	}

</script>
<script type="text/javascript"> 
 
function calcBMI(){
 
var weight = (isNaN(document.kmi.weight.value)) ? 1 : document.kmi.weight.value;
 
if (weight == 0) alert("Neteisingai nurodytas svoris.");
 
var height = (isNaN(document.kmi.height.value)) ? 1 : document.kmi.height.value;
 
if (height == 0) alert("Neteisingai nurodytas ūgis.");
var wmult = 1;
var hmult = .01;
var BMI = Math.round(((weight / wmult)/((height * hmult)*(height * hmult))) *10)/10;
 
var result = "";
if(BMI <18.5) result = "<b>nepakankamas</b>";
else if((BMI >=18.50)&&(BMI<25)) result = "<b>normalus</b>";
else if((BMI >=25)&&(BMI<30)) result = "<b>šiek kiek per didelis</b>";
else result = "<b>per didelis</b>";
 
document.getElementById('results').innerHTML = "Jūsų kūno masės indeksas yra: <b>" + BMI + "</b><br><br>Jūsų svoris yra " + result + ".";
}
</script>

<?php get_sidebar('kaire'); ?>
			<div id="content" style="width:775px">
			
			<div id="breadcrumb">
				<a href="<?php bloginfo( 'url' );?> " >Pradžia</a>  &rsaquo;  <a href=" <?php get_the_title() ?> " ><?php the_title()?></a>
			
</div>
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<div class="bloko_pavadinimas" style="width:775px">
	<h1 class="entry-title1"><?php the_title(); ?></h1>
	</div>
		<div id="tekstas">
	<?php the_content(); ?>
	</div>
	<?php endwhile; // end of the loop. ?>

			
		</div><!-- #container -->

<?php get_footer(); ?>
