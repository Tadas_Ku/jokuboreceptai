<?php
/*
Template Name: Receptu TOP

*/

?>
 
<?php get_header(); ?>
 <?php get_sidebar('kaire'); ?>
 			<div id="content">
			
						<div id="breadcrumb">
				<a href="<?php bloginfo( 'url' );?> " >Pradžia</a>  &rsaquo; 	<a href="/visi-receptai/" >Receptai</a>  &rsaquo;  <a href=" <?php get_the_title() ?> " ><?php the_title()?></a>
			
</div>
				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
			<div class="bloko_pavadinimas"> Receptų <?php the_title(); ?></div>
			<div id="trumpas_aprasymas1">
<?php the_content(); ?>
</div>
<div class="bloko_pavadinimas">Mėgstamiausi receptai </div>
<div class="receptai_tituliniam">
<?php geriausi_receptai1(); ?>	
</div>



	<?php endwhile; // end of the loop. ?>
 </div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
 
