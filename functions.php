<?php

// registruojam naujus sidebarus
function meniu_juostos() {


	register_sidebar( array(
		'name' => __( 'Kaires puses meniu', 'receptukas' ),
		'id' => 'kaire',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3 class="widget-title_kaire">',
		'after_title' => '</h3>',
	) );



	register_sidebar( array(
		'name' => __( 'Desines puses meniu', 'receptukas' ),
		'id' => 'desine',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

		register_sidebar( array(
		'name' => __( 'Footerio meniu-Turinys', 'receptukas' ),
		'id' => 'footer_turinys',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3 class="widget-title_footer">',
		'after_title' => '</h3>',
	) );
	
			register_sidebar( array(
		'name' => __( 'Footerio meniu-Apie', 'receptukas' ),
		'id' => 'footer_apie',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3 class="widget-title_footer">',
		'after_title' => '</h3>',
	) );

}
add_action( 'widgets_init', 'meniu_juostos' );

//Naujausi straipsniu blokelis tituliniam puslapiui

function naujausi_straipsniai() {
global $post;
$args = array( 'numberposts' => 5, 'post_type'=>'post', 'orderby'=> 'post_date' ,'order'=> 'DESC');
$myposts = get_posts( $args );
foreach( $myposts as $post ) :	setup_postdata($post); ?>
 <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
<?php endforeach;
}

//Geriausių receptų blokelis tituliniam puslapiui
function geriausi_receptai() {
global $wpdb ;
global $post;
$gettop = $wpdb->get_results("SELECT * FROM `wp_posts` LEFT JOIN wp_top_ten ON wp_posts.ID=wp_top_ten.postnumber 
where wp_posts.post_status='publish'  AND  wp_posts.post_type = 'receptai'
order by wp_top_ten.cntaccess DESC LIMIT 0,6 ") ;
foreach ($gettop as $post) :	
?>
<div class="recipe_promo"> 
<div class="recepto_foto">

 <?php if ( has_post_thumbnail()) : ?>
   <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
   <?php the_post_thumbnail('thumbnail'); ?>
   </a>
 <?php endif; ?>
</div>
<div class="recepto_foto_pavadinimas">
<a href="<?php the_permalink(); ?>"title="<?php the_title(); ?>">
<a href="<?php the_permalink() ?>">
<?php
$thetitle = $post->post_title;
$getlength = strlen($thetitle);
$thelength = 35;
echo substr($thetitle, 0, $thelength);
if ($getlength > $thelength) echo "...";
?>
</a>

</a>
</div>
</div>

<?php endforeach;

}

function geriausi_receptai1() {

global $wpdb ;
global $post;
$gettop = $wpdb->get_results("SELECT * FROM `wp_posts` LEFT JOIN wp_top_ten ON wp_posts.ID=wp_top_ten.postnumber 
where wp_posts.post_status='publish' AND  wp_posts.post_type = 'receptai'
order by wp_top_ten.cntaccess DESC LIMIT 0,30 ") ;
foreach ($gettop as $post) :	
?>
<div class="recipe_promo"> 
<div class="recepto_foto">

 <?php if ( has_post_thumbnail()) : ?>
   <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
   <?php the_post_thumbnail('thumbnail'); ?>
   </a>
 <?php endif; ?>
</div>
<div class="recepto_foto_pavadinimas">
<a href="<?php the_permalink(); ?>"title="<?php the_title(); ?>">
<a href="<?php the_permalink() ?>">
<?php
$thetitle = $post->post_title;
$getlength = strlen($thetitle);
$thelength = 35;
echo substr($thetitle, 0, $thelength);
if ($getlength > $thelength) echo "...";
?>
</a>

</a>
</div>
</div>

<?php endforeach;

}

function naujausi_receptai1() {

global $post;
$args = array( 'numberposts' => 20, 'post_type'=> 'receptai', 'orderby'=> 'post_date' ,'order'=> 'DESC');
$myposts = get_posts( $args );
foreach( $myposts as $post ) :	setup_postdata($post); ?>

<div class="recipe_promo"> 
<div class="recepto_foto">

 <?php if ( has_post_thumbnail()) : ?>
   <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
   <?php the_post_thumbnail('thumbnail'); ?>
   </a>
 <?php endif; ?>
</div>
<div class="recepto_foto_pavadinimas">
<a href="<?php the_permalink(); ?>"title="<?php the_title(); ?>">
<a href="<?php the_permalink() ?>">
<?php
$thetitle = $post->post_title;
$getlength = strlen($thetitle);
$thelength = 35;
echo substr($thetitle, 0, $thelength);
if ($getlength > $thelength) echo "...";
?>
</a>

</a>
</div>
</div>

<?php endforeach;

}


function naujausi_receptai() {

global $post;
$args = array( 'numberposts' => 6, 'post_type'=> 'receptai', 'orderby'=> 'post_date' ,'order'=> 'DESC');
$myposts = get_posts( $args );
foreach( $myposts as $post ) :	setup_postdata($post); ?>

<div class="recipe_promo"> 
<div class="recepto_foto">

 <?php if ( has_post_thumbnail()) : ?>
   <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
   <?php the_post_thumbnail('thumbnail'); ?>
   </a>
 <?php endif; ?>
</div>
<div class="recepto_foto_pavadinimas">
<a href="<?php the_permalink(); ?>"title="<?php the_title(); ?>">
<a href="<?php the_permalink() ?>">
<?php
$thetitle = $post->post_title;
$getlength = strlen($thetitle);
$thelength = 35;
echo substr($thetitle, 0, $thelength);
if ($getlength > $thelength) echo "...";
?>
</a>

</a>
</div>
</div>

<?php endforeach;

}

//susije receptai
// if media-screen < 800px {
//     $susije = 4;}

function susije_receptai() {
	
global $post;
$terms = get_the_terms($post->ID,'receptu-tipas'); 
 foreach( $terms as $term ) {
 	$all_terms[] = $term->slug;
}
$args = array('tax_query'=>array(array('taxonomy' => 'receptu-tipas','field' => 'slug','terms' => $all_terms)),'numberposts' => 4, 'post_type'=> 'receptai', 'exclude'=>$post->ID, 'orderby'=> 'rand',);
$myposts = get_posts( $args );
if($myposts){
	foreach( $myposts as $post ) :	setup_postdata($post); ?>

	<div class="recipe_promo11"> 
	<div class="recepto_foto">

	 <?php if ( has_post_thumbnail()) : ?>
	   <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
	   <?php the_post_thumbnail('thumbnail'); ?>
	   </a>
	 <?php endif; ?>
	</div>
	<div class="recepto_foto_pavadinimas">
	<a href="<?php the_permalink(); ?>"title="<?php the_title(); ?>">
	<a href="<?php the_permalink() ?>">
	<?php
	$thetitle = $post->post_title;
	$getlength = strlen($thetitle);
	$thelength = 35;
	echo substr($thetitle, 0, $thelength);
	if ($getlength > $thelength) echo "...";
	?>
	</a>

	</a>
	</div>
	</div>
	<?php
	 endforeach;
	 wp_reset_query();
	}
}
//meniu punktas  receptams
add_action('init', 'receptai_meniu');
 
function receptai_meniu() {
 
	$labels = array(
		'name' => _x('Receptai', 'post type general name'),
		'singular_name' => _x('Receptas', 'post type singular name'),
		'add_new' => _x('Naujas receptas', 'portfolio item'),
		'add_new_item' => __('Naujas receptas'),
		'edit_item' => __('Redaguoti recepta'),
		'new_item' => __('Naujas receptas'),
		'view_item' => __('Žiureti recepta'),
		'search_items' => __('Ieškoti recepto'),
		'not_found' =>  __('Deja, kolega, kol kas nieko nera. Pasistenk, kad daugiau tokios klaidos nereiketu rodyti. '),
		'not_found_in_trash' => __('Šiukšliadėžė tuščia'),
		'parent_item_colon' => ''
	);
 
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'menu_icon' => get_stylesheet_directory_uri() . '/article16.png',
		'rewrite' => array('slug' => 'receptai'),
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 4,
		'supports' => array('title','editor','thumbnail','comments')
	  ); 
 
	register_post_type( 'receptai' , $args );
        flush_rewrite_rules();
}

add_action('init', 'maisto_priedai');
 
function maisto_priedai() {
 
	$labels = array(
		'name' => _x('Maisto priedai', 'post type general name'),
		'singular_name' => _x('Maito priedas', 'post type singular name'),
		'add_new' => _x('Naujas priedas', 'portfolio item'),
		'add_new_item' => __('Naujas priedas'),
		'edit_item' => __('Redaguoti prieda'),
		'new_item' => __('Naujas priedas'),
		'view_item' => __('Žiurėti priedą'),
		'search_items' => __('Ieškoti priedo'),
		'not_found' =>  __('Nieko nera '),
		'not_found_in_trash' => __('Nieko nera'),
		'parent_item_colon' => ''
	);
 
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'menu_icon' => get_stylesheet_directory_uri() . '/article16.png',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 4,
		'supports' => array('title','editor','thumbnail')
	  ); 
 
	register_post_type( 'maisto-priedai' , $args );
        flush_rewrite_rules();
}

//kategoriju registraviams
register_taxonomy("produktai", array("receptai"), array("hierarchical" => true, "label" => "Produktai", "singular_label" => "Produktai", "rewrite" => true));
register_taxonomy("receptu-tipas", array("receptai"), array("hierarchical" => true, "label" => "Pagal tipą", "singular_label" => "Receptu tipai", "rewrite" => true));
register_taxonomy("saliu-virtuve", array("receptai"), array("hierarchical" => true, "label" => "Pagal virtuvę", "singular_label" => "Pagal virtuvę", "rewrite" => true));
register_taxonomy("gaminimo-budas", array("receptai"), array("hierarchical" => true, "label" => "Pagal gaminimo būdą", "singular_label" => "Gaminimo budas", "rewrite" => true));
register_taxonomy("patiekalu-produktai", array("receptai"), array("hierarchical" => true, "label" => "Pagal ingredientus", "singular_label" => "Iš ko gaminama", "rewrite" => true));
register_taxonomy("sventiniai-patiekalai", array("receptai"), array("hierarchical" => true, "label" => "Šventiniai patiekalai", "singular_label" => "Šventiniai patiekalai", "rewrite" => true));


//papildomu lauku receptui generaviamas
add_action("admin_init", "admin_init");
 
function admin_init(){
  add_meta_box("laikas", "Gaminimo laikas", "laikas", "receptai", "side", "low");
  add_meta_box('recipesBoxIngredients', 'Reikalingi produktai', 'recipesBoxIngredients', "receptai", 'normal', 'high');  
  add_meta_box('aprasymas', 'Aprašymas', 'aprasymas', "receptai", 'normal', 'low');  
  add_meta_box('seo_aprasymas', 'Seo aprasymas', 'seo_aprasymas', "post", 'normal', 'low');  
  add_meta_box('seo_aprasymas', 'Seo aprasymas', 'seo_aprasymas', "page", 'normal', 'low');

}
  
 function laikas(){
  global $post;
  $custom = get_post_custom($post->ID);
  $gaminimo_laikas = $custom["gaminimo_laikas"][0];
?>
  

    <label>Gaminimo laikas:</label>
	<input name="gaminimo_laikas" value="<?php echo $gaminimo_laikas; ?>" />
  <?php
} 

 function aprasymas(){
  global $post;
  $custom = get_post_custom($post->ID);
  $aprasymas = $custom["aprasymas"][0];
?>

 <p><label>Čia recepto aprašymas:</label><br />
  <textarea cols="50" rows="5" name="aprasymas"><?php echo $aprasymas; ?></textarea></p>

  <?php
} 
function seo_aprasymas(){
  global $post;
  $custom = get_post_custom($post->ID);
  $seo_aprasymas = $custom["seo_aprasymas"][0];
?>

 <p><label>Čia meta description</label><br />
  <textarea cols="50" rows="5" name="seo_aprasymas"><?php echo $seo_aprasymas; ?></textarea></p>

  <?php
}

function recipesBoxIngredients() {
  global $post;
  $meta = get_post_meta($post->ID,'_my_meta',TRUE);
  include('produktai.php');
}





//papildomu lauku saugojimas
add_action('save_post', 'save_details');
function save_details(){
  global $post; 
if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
return $post_id;
if (isset($_POST["gaminimo_laikas"]) && $_POST["gaminimo_laikas"] <> '') update_post_meta($post->ID, "gaminimo_laikas", $_POST["gaminimo_laikas"]);
if (isset($_POST["aprasymas"]) && $_POST["aprasymas"] <> '') update_post_meta($post->ID, "aprasymas", $_POST["aprasymas"]);
if (isset($_POST["_my_meta"]) && $_POST["_my_meta"] <> '') update_post_meta($post->ID, "_my_meta", $_POST["_my_meta"]) ;
if (isset($_POST["seo_aprasymas"]) && $_POST["seo_aprasymas"] <> '') update_post_meta($post->ID, "seo_aprasymas", $_POST["seo_aprasymas"]);


}
add_theme_support( 'nav-menus' );
register_nav_menu('main', 'Main Navigation Menu');
register_nav_menu('extra', 'Meniu A-Z sarasui');
add_theme_support( 'post-thumbnails' );
function my_wp_nav_menu_args( $args ) {
	$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'my_wp_nav_menu_args' );

// breadcrum formuojant kategorija

function breadcrum_for_category() {
$the_tax = get_taxonomy( get_query_var( 'taxonomy' ) );
switch ($the_tax->label) {
case  'Pagal tipą' :
echo '	<a href="/visi-receptai/pagal-tipa/">Pagal tipą</a> ' ;
break;
case  'Pagal gaminimo būdą' :
echo '	<a href="/visi-receptai/pagal-gaminimo-buda/">Pagal gaminimo-budą</a> ' ;
break;
case  'Pagal ingredientus' :
echo '	<a href="/visi-receptai/pagal-ingredientus/">Pagal ingredientus</a> ' ;
break;
case  'Produktai' :
echo '	<a href="/sudetines-dalys/">Sudėtines dalys</a> ' ;
break;
}
}



//funkcija automatiniam meta descriptionu generavimui

function meta_descriptions() {
global $post;

//aprasymas receptui
if (is_singular('receptai')) {
$seo = get_post_meta($post->ID, 'aprasymas', true);
$getlength = strlen($seo);
$thelength = 160;
if ($seo) {
echo strip_tags (substr($seo, 0, $thelength));
if ($getlength > $thelength)
{
 echo "...";

}
}
}

//aprasymas tituliniam


if (is_home()) {
$settings = get_option( 'sa_options', $sa_options );
echo $settings['home_descr']; 
}

//aprasymas maisto priedui
if (is_singular('maisto-priedai')){
$seo = strip_tags ($post->post_content);
$getlength = strlen($seo);
$thelength = 160;
if ($seo) {
echo strip_tags (substr($seo, 0, $thelength));
if ($getlength > $thelength)
{
 echo "...";

}
}
}
//aprasymas kategorijai
if (is_tax() ) {
$seo = category_description();
$getlength = strlen($seo);
$thelength = 160;
if ($seo) {
echo strip_tags (substr($seo, 0, $thelength));
if ($getlength > $thelength)
{
 echo "...";

}
}
}
//aprasymas straipsniui arba puslapiui
if((is_singular('post') or is_page()) && (get_post_meta($post->ID, 'seo_aprasymas', true)))
{
$seo=get_post_meta($post->ID, 'seo_aprasymas', true);
$getlength = strlen($seo);
$thelength = 160;
if ($seo) {
echo strip_tags (substr($seo, 0, $thelength));
if ($getlength > $thelength)
{
 echo "...";

}
}
//aprasymas straipsniui arba receptui kai nera aprsymo ir turi paimti pirmus 160 simboliu.
}
elseif (is_singular('post') or is_page())
{
$seo=$post->post_content;
$getlength = strlen($seo);
$thelength = 160;
if ($seo) {
echo strip_tags (substr($seo, 0, $thelength));
if ($getlength > $thelength)
{
 echo "...";

}

}
}
}

require_once ( get_template_directory() . '/options.php' );

//rodyti tik balsavimu zvaigzdutes

function the_ratings_static($start_tag = 'div') {
	global $id;
	$ratings_id = $id;
	echo "<$start_tag id=\"post-avg\" class=\"post-ratings\">".the_ratings_results1($ratings_id).'</'.$start_tag.'>';
			return;
}


function the_ratings_results1($post_id, $new_user = 0, $new_score = 0, $new_average = 0, $type = 0) {
	if($new_user == 0 && $new_score == 0 && $new_average == 0) {
		$post_ratings_data = null;
	} else {
		$post_ratings_data->ratings_users = $new_user;
		$post_ratings_data->ratings_score = $new_score;
		$post_ratings_data->ratings_average = $new_average;
	}
	$template_postratings_text = stripslashes(get_option('postratings_template_mostrated'));
return expand_ratings_template($template_postratings_text, $post_id, $post_ratings_data);
}

function ogg_image()
{
 if(is_singular('receptai')) 
 {
$imageArray = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail_name' );
$imageURL = $imageArray[0]; // here's the image url
echo $imageURL;
 } 
 if(is_home())
 {
    echo('http://jokuboreceptai.lt/receptukas/wp-content/themes/receptukas/images/logo.gif');
 }
}

function titlai()
{
/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );    
}